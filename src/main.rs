extern crate minifb;

use minifb::{Key, Window, WindowOptions, Scale, ScaleMode};

const WIDTH: usize = 320;
const HEIGHT: usize = 240;

fn minifb_color_from_u8_rgb(r: u8, g: u8, b: u8) -> u32 {
    let (r, g, b) = (r as u32, g as u32, b as u32);
    (r << 16) | (g << 8) | b
}

fn dibujar_cielo_tierra(buffer : &mut std::vec::Vec<u32>) {
    for i in buffer.iter_mut().take(WIDTH*HEIGHT/2){
        *i = 0xaabbccff;
    }
    for i in buffer.iter_mut().skip(WIDTH*HEIGHT/2).take(WIDTH*HEIGHT/2){
        *i = 0x8B451300;
    }
}

fn main() {
    let mut buffer: Vec<u32> = vec![0; WIDTH * HEIGHT];

    let mut window = Window::new(
        "Test - ESC toiter_mut exit",
        WIDTH,
        HEIGHT,
        WindowOptions {
            borderless: false,
            transparency: false,
            title: true,
            resize: false,
            scale: Scale::X4,
            scale_mode: ScaleMode::Stretch,
            topmost: false,
            none: false,
        },
    )
    .unwrap_or_else(|e| {
        panic!("{}", e);
    });

    // Limit to max ~60 fps update rate
    window.limit_update_rate(Some(std::time::Duration::from_micros(16600)));
    
    for i in buffer.iter_mut() {
        *i = 0; // write something more funny here!
    }

    
    
    while window.is_open() && !window.is_key_down(Key::Escape) {
        dibujar_cielo_tierra(&mut buffer);
        // We unwrap here as we want this code to exit if it fails. Real applications may want to handle this in a different way
        window
            .update_with_buffer(&buffer, WIDTH, HEIGHT)
            .unwrap();
    }
}